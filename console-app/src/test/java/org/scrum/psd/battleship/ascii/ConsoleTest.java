package org.scrum.psd.battleship.ascii;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConsoleTest {


    @Test
    public void shouldRenderCannon() {
        final Interface console = new Interface(null);

        final String result = console.CANNON_SPRITE;

        assertEquals("                  __\n" +
                "                 /  \\\n" +
                "           .-.  |    |\n" +
                "   *    _.-'  \\  \\__/\n" +
                "    \\.-'       \\\n" +
                "   /          _/\n" +
                "  |      _  /\" \"\n" +
                "  |     /_'\n" +
                "   \\    \\_/\n" +
                "    \" \"\" \"\" \"\" \"\n", result);
    }

}