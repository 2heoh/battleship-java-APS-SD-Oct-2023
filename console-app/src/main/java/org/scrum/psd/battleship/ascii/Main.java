package org.scrum.psd.battleship.ascii;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final Interface console = new Interface(System.out);
        BattleshipGame game = new BattleshipGame(new Telemetry(), console, scanner);
        game.startGame();
    }


}
