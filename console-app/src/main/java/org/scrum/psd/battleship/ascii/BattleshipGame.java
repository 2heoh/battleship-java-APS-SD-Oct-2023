package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BattleshipGame {
    private static final String QUIT = "Q";
    private final Telemetry telemetry;
    private final Interface anInterface;
    private final Scanner scanner;

    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;


    public BattleshipGame(Telemetry telemetry, Interface console, Scanner scanner) {
        this.telemetry = telemetry;
        this.anInterface = console;
        this.scanner = scanner;
    }

    public static List<Ship> InitializeMyFleet(Telemetry telemetry, Interface anInterface, Scanner scanner) {
        myFleet = GameController.initializeShips();

        anInterface.displayInfo("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            anInterface.displayInfo(String.format("\nPlease enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                anInterface.displayUserPrompt(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                String positionInput = scanner.next();
                ship.addPosition(positionInput);
                telemetry.trackEvent("Player_PlaceShipPosition", "Position", positionInput, "Ship", ship.getName(), "PositionInShip", Integer.valueOf(i).toString());
            }
        }
        return myFleet;
    }

    public static List<Ship> InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 9));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
        return enemyFleet;
    }

    static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        return new Position(letter, number);
    }

    private void StartGame(Interface anInterface, Scanner scanner) {
        anInterface.displayCannon();
        game:
        do {
            anInterface.displayInfo("\nPlayer, it's your turn");
            anInterface.displayInfo("You can do: [enter coordinates, q - quit]");
            anInterface.displayUserPrompt("Enter your command or coordinates for shot:");

            final String action = defineAction(scanner);
            switch (action) {
                case QUIT:
                    anInterface.displayInfo("Quit game, buy!");
                    break game;
                default:
                    makeRound(anInterface, parsePosition(action));
            }


        } while (true);
    }

    private void makeRound(Interface anInterface, Position position) {
        boolean isHit = GameController.checkIsHit(enemyFleet, position);
        if (isHit) {
            anInterface.beep();
            anInterface.displayBlast();
            anInterface.displayInfo("Yeah ! Nice hit !");
        } else {
            anInterface.displayMiss();
        }
        new Telemetry().trackEvent("Player_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());

        position = getRandomPosition();
        isHit = GameController.checkIsHit(myFleet, position);
        anInterface.displayInfo(String.format("\nComputer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss"));
        telemetry.trackEvent("Computer_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
        if (isHit) {
            anInterface.beep();
            anInterface.displayBlast();
        }
        anInterface.displayInfo("\n=====================================\n");
    }

    private static String defineAction(Scanner scanner) {
        String input = scanner.next();
        if(input.toUpperCase().charAt(0) == 'Q') {
            return "Q";
        } else {
            return input;
        }
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    public void startGame() {
        telemetry.trackEvent("ApplicationStarted", "Technology", "Java");
        anInterface.displayIntroShip();
        myFleet = InitializeMyFleet(telemetry, anInterface, scanner);
        enemyFleet = InitializeEnemyFleet();

        StartGame(anInterface, scanner);
    }
}
