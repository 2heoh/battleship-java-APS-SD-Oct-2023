package org.scrum.psd.battleship.ascii;

import java.io.PrintStream;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.*;

public class Interface {
    private final String BLAST_SPRITE =
        "                \\         .  ./\n" +
        "              \\      .:\" \";'.:..\" \"   /\n" +
        "                  (M^^.^~~:.'\" \").\n" +
        "            -   (/  .    . . \\ \\)  -\n" +
        "               ((| :. ~ ^  :. .|))\n" +
        "            -   (\\- |  \\ /  |  /)  -\n" +
        "                 -\\  \\     /  /-\n" +
        "                   \\  \\   /  /\n";
    public final String MISS = "Miss";
    public final String CANNON_SPRITE =
            "                  __\n" +
            "                 /  \\\n" +
            "           .-.  |    |\n" +
            "   *    _.-'  \\  \\__/\n" +
            "    \\.-'       \\\n" +
            "   /          _/\n" +
            "  |      _  /\" \"\n" +
            "  |     /_'\n" +
            "   \\    \\_/\n" +
            "    \" \"\" \"\" \"\" \"\n";
    private final PrintStream outStream;
//    private StringCollector out = new StringCollector();
    private final String SHIP_SPRITE  =
        "                                     |__\n" +
        "                                     |\\/\n"+
        "                                     ---\n"+
        "                                     / | [\n"+
        "                              !      | |||\n"+
        "                            _/|     _/|-++'\n"+
        "                        +  +--|    |--|--|_ |-\n"+
        "                     { /|__|  |/\\__|  |--- |||__/\n"+
        "                    +---------------___[}-_===_.'____                 /\\\n"+
        "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _\n"+
        " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7\n"+
        "|                        Welcome to Battleship                         BB-61/\n"+
        " \\_________________________________________________________________________|\n";

    public Interface(PrintStream out) {
        outStream = out;
    }

    public void displayIntroShip() {
        outStream.println(colorize(SHIP_SPRITE, MAGENTA_TEXT()));
    }

    void beep() {
        outStream.print("\007");
    }

    void displayBlast() {
        outStream.println(colorize(BLAST_SPRITE, RED_TEXT()));
    }

    void displayMiss() {
        outStream.println(colorize( MISS, BLUE_BACK()));
    }

    void displayInfo(String message) {
        outStream.println(message);
    }

    void displayCannon() {
        outStream.print(colorize(CANNON_SPRITE, BLUE_TEXT()));
    }

    void displayUserPrompt(String message) {
        outStream.println(colorize(message, YELLOW_TEXT()));
    }

}
